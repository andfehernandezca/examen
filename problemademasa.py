import math as m

class metodos:

    def movimiento(self,angulo,cr,m1):
        list=[]
        m2=0
        aux=-m1*9.8*(m.sin(m.radians(angulo))+cr*m.cos(angulo))
        list.append([m2,"subiendo"])
        while aux<0:
            m2+=0.5
            aux=m2*9.8-(m1*9.8*(m.sin(m.radians(angulo))+cr*m.cos(angulo)))
            list.append([m2,"subiendo"])
        list.append([m2,"bajando"])
        return list

    def angulo(self,m1,m2,cr):
        resp=-1
        aux=0
        for i in range(50):
            aux=round(m2*9.8-(m1*9.8*(m.sin(m.radians(i+10))+cr*m.cos(m.radians(i+10)))),1)
            if aux==0:
                resp=i+10
        return resp

a=metodos()

print(a.movimiento(55,0.15,6))
print("su angulo es:")
print(a.angulo(8,6,0.2))
